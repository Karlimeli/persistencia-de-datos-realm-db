package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {


    @Required
    private String name;
    private String lastname;
    private String age;

    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _name, String _lastname, String _age) {
        this.name = _name; this.lastname = _lastname; this.age = _age;
    }

    public Task() {}

    public void setStatus(TaskStatus status) {
        this.status = status.name();
    }
    public String getStatus() {
        return this.status;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }


}
